#!/bin/sh

DYNASOR=dynasor

K_BINS=100			# Number of bins spaced evenly between Gamma and the specified k-point.
K_POINTS=100		# Number of kpoints spaced evenly between Gamma and the specified k-point.
TIME_WINDOW=2000    # Consider time correlations up to TIME_WINDOW trajectory frames
MAX_FRAMES=20000   	# Read at most MAX_FRAMES frames from trajectory file (then stop)

dt=$((5*5)) # This needs to be correspond to lammps timestep * dumpFreq * $STEP in fs in order to get the units correct.


# K - points for T=300 
# 
# L point  k = 7.62,7.62,7.62
# K point  k = 11.43,11.43,0
#



TRAJECTORY="lammpsrun/dump/dumpT300.NVT.atom.velocity"

OUTPUT="outputs/dynasor_outT300_GK"
echo "\nRunning Gamma to K\n\n"

${DYNASOR} -f "$TRAJECTORY" \
	--k-bins=$K_BINS \
	--k-points=$K_POINTS \
	--max-frames=$MAX_FRAMES \
    --om=$OUTPUT.m \
	--nt=$TIME_WINDOW \
	--dt=$dt \
	--k-sampling="line" \
	--k-direction=11.43,11.43,0


OUTPUT="outputs/dynasor_outT300_GL"
echo "\nRunning Gamma to K\n\n"

${DYNASOR} -f "$TRAJECTORY" \
	--k-bins=$K_BINS \
	--k-points=$K_POINTS \
	--max-frames=$MAX_FRAMES \
    --om=$OUTPUT.m \
	--nt=$TIME_WINDOW \
	--dt=$dt \
	--k-sampling="line" \
	--k-direction=7.62,7.62,7.62



