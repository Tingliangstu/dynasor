# -*- coding: utf-8 -*-

"""
dynasor module.
"""

__project__ = 'dynasor'
__description__ = 'A tool for calculating dynamical structure factors'
__copyright__ = '2021'
__license__ = 'MIT'
__credits__ = ['The dynasor developers team']
__version__ = '1.1.1'
__maintainer__ = 'The dynasor developers team'
__maintainer_email__ = 'dynasor@materialsmodeling.org'
__status__ = 'Development Status :: 5 - Production/Stable'
__url__ = 'http://dynasor.materialsmodeling.org/'
